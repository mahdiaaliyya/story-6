from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.contrib.staticfiles.storage import staticfiles_storage
from django.contrib.staticfiles import finders
from django.contrib.auth.models import User
from .views import login_user, logout_user, signup
from django.http import HttpRequest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time
# Create your tests here.


class story9unitTests(TestCase):
    def test_login_url_is_resolved(self):
        response = Client().get('/user/login/')
        self.assertEqual(response.status_code, 200)

    def test_signup_url_is_resolved(self):
        response = Client().get('/user/signup/')
        self.assertEqual(response.status_code, 200)

    def test_login_template_are_correct(self):
        response = Client().get('/user/login/')
        self.assertTemplateUsed(response, 'login.html')

    def test_signup_template_are_correct(self):
        response = Client().get('/user/signup/')
        self.assertTemplateUsed(response, 'signup.html')

    def test_using_about_func_login(self):
        found = resolve('/user/login/')
        self.assertEqual(found.func, login_user)

    def test_using_about_func_signup(self):
        found = resolve('/user/signup/')
        self.assertEqual(found.func, signup)

    def test_using_about_func_logout(self):
        found = resolve('/user/logout/')
        self.assertEqual(found.func, logout_user)

    def test_html_element_login_exist(self):
        request = HttpRequest()
        response = login_user(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Log In', html_response)

    def setUp(self):
        u = User.objects.create(username="testuname")
        u.set_password("ppwauthetication")
        u.save()

    def test_user_object(self):
        user_test = User.objects.get(username="testuname")
        self.assertEquals(user_test.username, 'testuname')

    def test_response_for_signup_valid(self):
        c = Client()
        response = c.post('/user/signup/', username="iniunittest",
                          password1="ppwauthentication", password2="ppwauthentication")
        self.assertTrue(response)


class functionalTest(LiveServerTestCase):

    def setUp(self):
        super().setUp()
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome(
            "./chromedriver", chrome_options=chrome_options)

    def tearDown(self):
        self.browser.quit()
        super().tearDown()

    def test_website(self):
        self.browser.get(self.live_server_url + '/user/signup/')
        self.assertIn('Sign Up', self.browser.page_source)
        self.browser.find_element_by_name("signupbtn").click()
        time.sleep(3)
        self.browser.find_element_by_name(
            "username").send_keys("testuname")
        time.sleep(3)
        self.browser.find_element_by_name(
            "password1").send_keys("ppwauthentication")
        time.sleep(3)
        self.browser.find_element_by_name(
            "password2").send_keys("ppwauthentication")
        time.sleep(3)
        self.browser.find_element_by_name("signupbtn").click()
        time.sleep(3)
        self.assertIn('Log In', self.browser.page_source)
        self.browser.find_element_by_name(
            "username").send_keys("testuname")
        time.sleep(3)
        self.browser.find_element_by_name(
            "password").send_keys("ppwauthentication")
        time.sleep(3)
        self.browser.find_element_by_name("loginbtn").click()
        time.sleep(3)
        self.assertIn('Welcome back, testuname',
                      self.browser.page_source)
        self.browser.find_element_by_name("logoutbtn").click()
        time.sleep(5)
