$(document).ready(function () {
    $("#book-input").keyup(function (event) {
        if (event.keyCode === 13) {
            $("#search-button").click();
        }
    });

    $("#search-button").on("click", function (e) {
        var q = $("#book-input").val()
        console.log(q)
        $.ajax({
            url: "https://www.googleapis.com/books/v1/volumes?q=" + q,
            success: function (data) {
                $('#table-content').html('')
                var result = '<tr>';
                for (var i = 0; i < data.items.length; i++) {

                    result += "<tr> <th>" + (i + 1) + "</th>";
                    if ((typeof (data.items[i].volumeInfo.imageLinks)) === 'undefined') {
                        result += "<td> No Thumbnail </td>";
                    } else {
                        result += "<td><img class='img-fluid' style='width:22vh' src='" + data.items[i].volumeInfo.imageLinks.smallThumbnail + "'></img>" + "</td>";
                    }

                    result += "<td class='align-middle'>" + data.items[i].volumeInfo.title + "</td>";
                    if ((typeof (data.items[i].volumeInfo.authors)) === 'undefined') {
                        result += 'No Author';
                    } else {
                        result += "<td class='align-middle'>" + data.items[i].volumeInfo.authors + "</td>"
                    }
                    result += "<td class='align-middle'>" + data.items[i].volumeInfo.publisher + "</td>" +
                        "<td class='align-middle'>" + data.items[i].volumeInfo.publishedDate + "</td>"
                }
                $('#table-content').append(result);
            },
            error: function (error) {
                alert("Books not found");
            }
        })
    });
});
