from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.http import HttpRequest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from .views import index
import time


# Create your tests here.
class LibraryTest(TestCase):

    def test_page(self):
        response = Client().get("/library/")
        self.assertEqual(response.status_code, 200)

    def test_page_template(self):
        response = Client().get('/library/')
        self.assertTemplateUsed(response, 'library.html')

    def test_json_url(self):
        response = Client().get('/library/data/')
        self.assertEqual(response.status_code, 200)

    def test_header(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn("Digital", html_response)

    def test_function_used(self):
        found = resolve('/library/')
        self.assertEqual(found.func, index)


class FuncionalTest(LiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.selenium = webdriver.Chrome(
            './chromedriver', chrome_options=chrome_options)
        self.selenium.get('http://localhost:8000/library')

    def tearDown(self):
        self.selenium.quit()

    def test_search_library(self):
        self.selenium.get(self.live_server_url + '/library/')
        self.assertIn("Digital Library", self.selenium.page_source)
        self.assertIn('<table',
                      self.selenium.page_source)
        time.sleep(3)
        self.selenium.find_element_by_name(
            'book-input').send_keys('programming')
        time.sleep(5)
        self.selenium.find_element_by_id('search-button').click()
        time.sleep(5)
        self.assertIn('<td>', self.selenium.page_source)
        time.sleep(3)
