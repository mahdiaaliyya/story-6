from django.shortcuts import render
from django.http import JsonResponse
import requests
import json


def index(request):
    return render(request, 'library.html')


def data(request):
    try:
        q = request.GET['q']

    except:
        q = 'quilting'

    json_read = requests.get(
        'https://www.googleapis.com/books/v1/volumes?q=' + q).json()
    return JsonResponse(json_read)
