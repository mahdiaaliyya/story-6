from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='library'),
    path('data/', views.data, name='library-data'),
]
