$('.toggle').click(function (e) {
    e.preventDefault();

    var $this = $(this);

    if ($this.next().hasClass('show')) {
        $this.next().removeClass('show');
        $this.next().slideUp(350);
    } else {
        $this.parent().parent().find('.inner').removeClass('show');
        $this.parent().parent().find('.inner').slideUp(350);
        $this.next().toggleClass('show');
        $this.next().slideToggle(350);
    }
});

$('#black').click(setDarkTheme);
$('#white').click(setDefaultTheme);
$('#green').click(setGreenTheme);

function setDarkTheme() {
    $('body').attr('data-theme', 'dark');
}

function setDefaultTheme() {
    $('body').attr('data-theme', 'default');
}

function setGreenTheme() {
    $('body').attr('data-theme', 'green');
}