from django import forms


class CreateStatus(forms.Form):
    status = forms.CharField(max_length=300)
