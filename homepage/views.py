from django.shortcuts import render, redirect
from .models import Status
from .forms import CreateStatus

# Create your views here.


def index(request):
    if request.method == 'POST':
        form = CreateStatus(request.POST)
        if form.is_valid():
            stat = Status(
                status=form.data["status"],
            )
            stat.save()

    else:
        form = CreateStatus()

    statuses = Status.objects.all()

    context = {
        'form': form,
        'statuses': statuses,
    }
    return render(request, 'index.html', context)
