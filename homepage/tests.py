from django.test import TestCase, LiveServerTestCase, Client
from django.urls import resolve
from .views import index
from django.utils import timezone
from .models import Status
from .forms import CreateStatus
from selenium import webdriver
import time


class UnitTest(TestCase):
    # URL TEST
    def test_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_story6_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    #
    def test_index_template(self):
        response = Client().get('/')
        self.assertContains(response, 'Halo, apa kabar?', html=True)
        self.assertContains(response, '<form')
        self.assertContains(
            response,
            '<input type="text" name="status" id="id_status" maxlength="300" required>',
            html=True,
        )
        self.assertContains(response, 'type="submit"')

    # TEST MODEL
    def test_status_model(self):
        Status.objects.create(status="New Status", submit_time=timezone.now())
        countStatuses = Status.objects.count()
        self.assertEqual(countStatuses, 1)

    # FORM POST TEST
    def test_form_post(self):
        response = self.client.post('/', follow=True, data={
            'status': 'coba isi status',
        })

        self.assertEqual(Status.objects.count(), 1)

        today = timezone.now()
        status = Status.objects.first()

        self.assertEqual(status.submit_time.date(), today.date())

    def test_views_render_the_correct_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_submit_empty_status(self):
        form = CreateStatus(data={'status': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['status'], ['This field is required.'])

    def test_submitted_status_shows_up(self):
        response = self.client.post('/', data={'status': 'belajar tdd'})
        self.assertContains(response, 'belajar tdd')


class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.driver = webdriver.Chrome(
            chrome_options=chrome_options, executable_path='./chromedriver')

    def tearDown(self):
        self.driver.quit()
        super().tearDown()

    def test_status_functional(self):
        self.driver.get(self.live_server_url + '/')
        time.sleep(3)
        self.assertInHTML('Halo, apa kabar?', self.driver.page_source)
        status = self.driver.find_element_by_name('status')
        button = self.driver.find_element_by_name('submit')
        status.send_keys('Coba coba')
        button.click()
        self.assertIn('Coba coba', self.driver.page_source)

    # Test story 7
    def test_accordion(self):
        self.driver.get(self.live_server_url + '/')
        time.sleep(3)
        self.driver.find_element_by_id('accordion-1').click()
        time.sleep(3)
        self.assertIn("I code", self.driver.page_source)
        time.sleep(3)
        self.driver.find_element_by_id('accordion-2').click()
        time.sleep(3)
        self.assertIn("IT Force FUKI", self.driver.page_source)
        time.sleep(3)
        self.driver.find_element_by_id('accordion-3').click()
        time.sleep(3)
        self.assertIn("Surviving pacil", self.driver.page_source)

    def test_change_theme(self):
        self.driver.get(self.live_server_url + '/')
        time.sleep(3)
        self.driver.find_element_by_id("green").click()
        # time.sleep(3)
        bg_color = self.driver.find_element_by_tag_name(
            'body').value_of_css_property('background-color')
        time.sleep(3)
        self.assertIn('rgba(95, 158, 160, 1)', bg_color)
        time.sleep(3)
        self.driver.find_element_by_id("white").click()
        # time.sleep(3)
        bg_color = self.driver.find_element_by_tag_name(
            'body').value_of_css_property('background-color')
        time.sleep(3)
        self.assertIn('rgba(255, 255, 255, 1)', bg_color)
        time.sleep(3)
        self.driver.find_element_by_id('black').click()
        # time.sleep(3)
        bg_color = self.driver.find_element_by_tag_name(
            'body').value_of_css_property('background-color')
        time.sleep(3)
        self.assertIn('rgba(0, 0, 0, 1)', bg_color)
